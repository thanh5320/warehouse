package com.hust.movie_review.data;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;

@Data
@Accessors(chain = true)
public class ProductInfo {
    private String code;

    private String name;

    private Double length;

    private Double width;

    private Double height;

    private Double weight;
}
