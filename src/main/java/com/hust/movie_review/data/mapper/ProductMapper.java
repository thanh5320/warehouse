package com.hust.movie_review.data.mapper;

import com.hust.movie_review.data.ProductInfo;
import com.hust.movie_review.models.Product;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductMapper {
    public static Product toPOJO(ProductInfo source){
        return new Product().setCode(source.getCode())
            .setHeight(source.getHeight())
            .setLength(source.getLength())
            .setName(source.getName())
            .setWeight(source.getWeight())
            .setWidth(source.getWidth())
            .setCreatedAt(new Date());
    }

    public static List<Product> toPOJOs(List<ProductInfo> productInfos){
        return Optional.ofNullable(productInfos).orElse(new ArrayList<>())
            .stream()
            .map(ProductMapper::toPOJO)
            .collect(Collectors.toList());
    }
}
