package com.hust.movie_review.data.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Listing<T> {
    public int total;
    public List<T> hits;
}
