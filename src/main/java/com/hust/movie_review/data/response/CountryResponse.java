package com.hust.movie_review.data.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.Set;

@Data
@Accessors(chain = true)
public class CountryResponse {
    private String code;

    private String name;

    private String continent;

    private Set<ActorResponse> actors;

    private Date createdAt;

    private Date updateAt;
}
