package com.hust.movie_review.data.response;

import com.hust.movie_review.data.ProductInfo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ImportOrderRequest {
    private String code;
    private List<ProductInfo> products;
}
