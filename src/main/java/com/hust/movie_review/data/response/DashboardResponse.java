package com.hust.movie_review.data.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DashboardResponse {
    int totalImportOrder = 0;
    int totalExportOrder = 0;
    int totalCellNull = 0;
    int totalCellNotNull = 0;
}
