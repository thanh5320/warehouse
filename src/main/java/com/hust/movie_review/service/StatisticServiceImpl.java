package com.hust.movie_review.service;

import com.hust.movie_review.data.response.DashboardResponse;
import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.Order;
import com.hust.movie_review.repositories.ICellRepository;
import com.hust.movie_review.repositories.IOrderRepository;
import com.hust.movie_review.service.template.IStatisticService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticServiceImpl implements IStatisticService {
    @Autowired
    public ICellRepository cellRepository;

    @Autowired
    public IOrderRepository orderRepository;

    @Override
    public List<Cell> statisticCells() {
        return cellRepository.findAll();
    }

    @Override
    public DashboardResponse dashboard() {
        int totalCellNull = 0;
        int totalCellNotNull = 0;
        List<Cell> cells = cellRepository.findAll();
        for(Cell cell: cells){
            if(cell.getProducts()!=null && !cell.getProducts().isEmpty()) totalCellNotNull++;
            else totalCellNull++;
        }

        int totalImportOrder = 0;
        int totalExportOrder = 0;
        List<Order> orders = orderRepository.findAll();
        for(Order order: orders){
            if(order.getOrderType().getName().equals("import")) totalImportOrder++;
            else totalExportOrder++;
        }

        return new DashboardResponse().setTotalCellNull(totalCellNull)
            .setTotalCellNotNull(totalCellNotNull)
            .setTotalExportOrder(totalExportOrder)
            .setTotalImportOrder(totalImportOrder);
    }
}
