package com.hust.movie_review.service.template;

import com.hust.movie_review.data.response.DashboardResponse;
import com.hust.movie_review.models.Cell;

import java.util.List;

public interface IStatisticService {
    List<Cell> statisticCells();
    DashboardResponse dashboard();
}
