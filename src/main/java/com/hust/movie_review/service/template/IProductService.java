package com.hust.movie_review.service.template;

import com.hust.movie_review.data.response.Listing;
import org.springframework.web.bind.annotation.RequestParam;

public interface IProductService {
    public Listing listing(Integer page,
                           Integer size);
}
