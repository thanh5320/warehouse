package com.hust.movie_review.service.template;

import com.hust.movie_review.data.response.ExportOrderRequest;
import com.hust.movie_review.data.response.ImportOrderRequest;
import com.hust.movie_review.data.response.Listing;
import org.springframework.web.bind.annotation.RequestParam;

public interface IOderService {
    public String importOrder(ImportOrderRequest request);
    public String exportOrder(ExportOrderRequest request);

    public Listing listing(String orderType,
                           Integer page,
                           Integer size);
}
