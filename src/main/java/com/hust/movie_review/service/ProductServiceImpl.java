package com.hust.movie_review.service;

import com.hust.movie_review.common.OffsetBasedPageable;
import com.hust.movie_review.data.response.Listing;
import com.hust.movie_review.models.Product;
import com.hust.movie_review.repositories.IProductRepository;
import com.hust.movie_review.service.template.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    IProductRepository productRepository;

    @Override
    public Listing listing(Integer page, Integer size) {
        Pageable pageable = new OffsetBasedPageable(page, size);
        Page<Product> productsPage = productRepository.findProductByCellNotNull(pageable);
        return new Listing().setTotal((int) productsPage.getTotalElements())
            .setHits(productsPage.getContent());
    }

}

