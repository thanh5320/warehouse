package com.hust.movie_review.service;

import com.hust.movie_review.common.Heuristic;
import com.hust.movie_review.common.OffsetBasedPageable;
import com.hust.movie_review.config.exception.ApiException;
import com.hust.movie_review.data.ProductInfo;
import com.hust.movie_review.data.mapper.ProductMapper;
import com.hust.movie_review.data.response.ExportOrderRequest;
import com.hust.movie_review.data.response.ImportOrderRequest;
import com.hust.movie_review.data.response.Listing;
import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.Order;
import com.hust.movie_review.models.OrderType;
import com.hust.movie_review.models.Product;
import com.hust.movie_review.repositories.ICellRepository;
import com.hust.movie_review.repositories.IOrderRepository;
import com.hust.movie_review.repositories.IOrderTypeRepository;
import com.hust.movie_review.repositories.IProductRepository;
import com.hust.movie_review.service.template.IOderService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.hust.movie_review.data.mapper.ProductMapper.toPOJOs;

@Service
public class OrderServiceImpl implements IOderService {
    @Autowired
    private ICellRepository cellRepository;

    @Autowired
    private IOrderRepository orderRepository;

    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private IOrderTypeRepository orderTypeRepository;

    @SneakyThrows
    @Transactional
    public String importOrder(ImportOrderRequest request){
        List<Cell> allCell = cellRepository.findAll();
        List<Product> newProducts = toPOJOs(request.getProducts());
        Collections.sort(newProducts, Collections.reverseOrder());

    for(Product product : newProducts){
        boolean b = false;
            Collections.sort(allCell, Cell::compareTo);
            for(Cell cell: allCell){
                if(Heuristic.isFit(cell, product)){
                    cell.addProductToCell(product);
                    product.setCell(cell);
                    b=true;
                    break;
                }
            }
            if(!b) throw new ApiException("Đơn hàng không thể nhập kho vì quá kích thước");
        }


        Order order = new Order()
            .setDateImport(new Date())
            .setOrderType(orderTypeRepository.findByName("import"))
            .setCode(request.getCode())
            .setCreatedAt(new Date());

        Order orderSave = orderRepository.save(order);

        for(Product product : newProducts) {
            product.setOrderImport(orderSave);
            productRepository.save(product);
        }
        return "success";
    }

    @Override
    @Transactional
    public String exportOrder(ExportOrderRequest request) {
        Order order = new Order()
            .setDateExport(new Date())
            .setOrderType(orderTypeRepository.findByName("export"))
            .setCode(request.getCode())
            .setCreatedAt(new Date());

        Order orderSave = orderRepository.save(order);

        List<Product> products = productRepository.findByIdIn(request.getProductIds());

        for(Product product: products){
            productRepository.save(product.setCell(null).setOrderExport(orderSave));
        }
        return "success";
    }

    @Override
    public Listing listing(String orderTypeStr, Integer page, Integer size) {
        OrderType orderType = orderTypeRepository.findByName(orderTypeStr);
        Pageable pageable = new OffsetBasedPageable(page, size);
        Page<Order> orderPage = orderRepository.findProductByOrderType(pageable, orderType);
        return new Listing().setTotal((int) orderPage.getTotalElements())
            .setHits(orderPage.getContent());
    }
}
