package com.hust.movie_review.common;

import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.Product;

public class Heuristic {
    public static boolean isFit(Cell cell, Product product){
        Double totalS = product.getWidth()*product.getLength();
        Double totalW = product.getWeight();
        for(Product productInCell: cell.getProducts()){
            totalS += productInCell.getWeight()* productInCell.getLength();
            totalS += productInCell.getWeight();
        }
        if(totalW>cell.getWeight() || totalS > cell.getWidth()* cell.getLength()){
            return false;
        }
        return true;
    }

}
