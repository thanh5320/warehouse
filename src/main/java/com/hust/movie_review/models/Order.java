package com.hust.movie_review.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Accessors(chain = true)
@Table(name = "orders")
@Entity
@ToString(exclude ={"orderType", "productsImport", "productsExport"})
@EqualsAndHashCode(exclude = {"orderType", "productsImport", "productsExport"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "code", nullable = true)
    private String code;

    @Column(name = "date_import")
    private Date dateImport;

    @Column(name = "date_export")
    private Date dateExport;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updateAt;

    @ManyToOne
    @JoinColumn(name = "order_type_id", nullable = false)
    @JsonBackReference
    private OrderType orderType;

    @OneToMany(mappedBy="orderImport")
    private Set<Product> productsImport;

    @OneToMany(mappedBy="orderExport")
    private Set<Product> productsExport;
}
