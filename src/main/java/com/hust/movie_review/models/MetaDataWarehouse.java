package com.hust.movie_review.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Accessors(chain = true)
@Table(name = "meta_data_warehouses")
@Entity
@ToString(exclude ={"cells"})
@EqualsAndHashCode(exclude = {"cells"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class MetaDataWarehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "total_row", nullable = true)
    private int totalRow;

    @Column(name = "total_column", nullable = true)
    private int totalColumn;

    @Column(name = "total_high", nullable = true)
    private int totalHigh;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updateAt;

    @OneToMany(mappedBy="warehouse")
    private Set<Cell> cells;
}
