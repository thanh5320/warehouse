package com.hust.movie_review.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "products")
@Entity
@ToString(exclude ={"orderImport", "orderExport", "cell"})
@EqualsAndHashCode(exclude = {"orderImport", "orderExport", "cell"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Product implements Comparable<Product>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "code", nullable = true)
    private String code;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "length", nullable = true)
    private Double length;

    @Column(name = "width", nullable = true)
    private Double width;

    @Column(name = "height", nullable = true)
    private Double height;

    @Column(name = "weight", nullable = true)
    private Double weight;

    @ManyToOne
    @JoinColumn(name = "cell_id", nullable = true)
    @JsonBackReference
    private Cell cell;

    @ManyToOne
    @JoinColumn(name = "order_import_id", nullable = true)
    @JsonBackReference
    private Order orderImport;

    @ManyToOne
    @JoinColumn(name = "order_export_id", nullable = true)
    @JsonBackReference
    private Order orderExport;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updateAt;

    @Override
    public int compareTo(Product product) {
        if(width*length < product.getLength()*product.getWidth())
            return -1;
        else if (width*length > product.getLength()*product.getWidth())
            return 1;
        else return 0;
    }
}
