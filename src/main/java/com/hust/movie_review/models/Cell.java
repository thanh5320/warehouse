package com.hust.movie_review.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Accessors(chain = true)
@Table(name = "cells")
@Entity
@ToString(exclude ={"warehouse", "products"})
@EqualsAndHashCode(exclude = {"warehouse", "products"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Cell implements Comparable<Cell> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "code", nullable = true)
    private String code;


    @Column(name = "index_row", nullable = true)
    private int indexRow;

    @Column(name = "index_column", nullable = true)
    private int indexColumn;

    @Column(name = "index_high", nullable = true)
    private int indexHigh;

    @Column(name = "length", nullable = true)
    private Double length;

    @Column(name = "width", nullable = true)
    private Double width;

    @Column(name = "weight", nullable = true)
    private Double weight;

    @Column(name = "height", nullable = true)
    private Double height;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updateAt;

    @ManyToOne
    @JoinColumn(name = "warehouse_id", nullable = true)
    @JsonBackReference
    public MetaDataWarehouse warehouse;

    @OneToMany(mappedBy="cell")
    private Set<Product> products;

    @Override
    public int compareTo(Cell cell) {
        if(computeS(this) < computeS(cell))
            return -1;
        else if (computeS(this) > computeS(cell))
            return 1;
        else return 0;
    }

    public static Double computeS(Cell cell){
        double totalS = 0;
        for(Product productInCell: cell.getProducts()){
            totalS += productInCell.getWeight()* productInCell.getLength();
        }
        return totalS;
    }

    public void addProductToCell(Product product){
        products.add(product);
    }
}
