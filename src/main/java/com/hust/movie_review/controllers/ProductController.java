package com.hust.movie_review.controllers;

import com.hust.movie_review.data.response.DfResponse;
import com.hust.movie_review.data.response.Listing;
import com.hust.movie_review.service.template.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("product")
public class ProductController {
    @Autowired
    IProductService productService;
    @GetMapping("/listing")
    public DfResponse<Listing> listing(@RequestParam Integer page,
                                              @RequestParam Integer size){

        return DfResponse.okEntity(
            productService.listing(page, size)
        );
    }
}
