package com.hust.movie_review.controllers;

import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.MetaDataWarehouse;
import com.hust.movie_review.repositories.ICellRepository;
import com.hust.movie_review.repositories.IMetaDataWarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Transactional
public class Start {
    @Autowired
    private IMetaDataWarehouseRepository metaDataWarehouseRepository;

    @Autowired
    private ICellRepository cellRepository;

    private static final int totalRow = 5;
    private static final int totalColumn = 3;
    private static final int totalHigh = 2;
    private static final Double weight = 20d; // kg
    private static final Double length = 100d; // cm
    private static final Double width = 70d; // cm
    private static final Double height = 60d; //cm

    public void createMetadata() {
        MetaDataWarehouse metaDataWarehouse = new MetaDataWarehouse()
            .setName("Kho TP.HCM")
            .setCreatedAt(new Date())
            .setTotalColumn(totalColumn)
            .setTotalHigh(totalHigh)
            .setTotalRow(totalRow);

        MetaDataWarehouse dataWarehouseSave = metaDataWarehouseRepository.save(metaDataWarehouse);

        List<Cell> cells = new ArrayList<>();

        for (int i = 1; i <= totalRow; i++) {
            for (int j = 1; j <= totalColumn; j++) {
                for (int k = 1; k <= totalHigh; k++) {
                    Cell cell = new Cell()
                        .setCreatedAt(new Date())
                        .setCode("R" + i + "C" + j + "H" + k)
                        .setHeight(height)
                        .setLength(length)
                        .setWidth(width)
                        .setWeight(weight)
                        .setIndexRow(i)
                        .setIndexColumn(j)
                        .setIndexHigh(k)
                        .setWarehouse(dataWarehouseSave);

                    cells.add(cell);
                }
            }
        }
        cellRepository.saveAll(cells);
    }
}
