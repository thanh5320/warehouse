package com.hust.movie_review.controllers;

import com.hust.movie_review.data.response.DashboardResponse;
import com.hust.movie_review.data.response.DfResponse;
import com.hust.movie_review.models.Cell;
import com.hust.movie_review.service.template.IStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/statistic")
public class StatisticController {
    @Autowired
    private IStatisticService statisticService;

    @GetMapping("/warehouse")
    public DfResponse<List<Cell>> statisticCells(){
        return DfResponse.okEntity(
            statisticService.statisticCells()
        );
    }

    @GetMapping("/dashboard")
    public DfResponse<DashboardResponse> dashboard(){
        return DfResponse.okEntity(
            statisticService.dashboard()
        );
    }
}
