package com.hust.movie_review.controllers;

import com.hust.movie_review.data.response.*;
import com.hust.movie_review.service.template.IOderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    public IOderService oderService;

    @PostMapping("/import")
    public DfResponse<String> importOrder(@RequestBody @Valid ImportOrderRequest request){
        return DfResponse.okEntity(
                oderService.importOrder(request)
        );
    }

    @PostMapping("/export")
    public DfResponse<String> exportOrder(@RequestBody @Valid ExportOrderRequest request){
        return DfResponse.okEntity(
            oderService.exportOrder(request)
        );
    }

    @GetMapping("/listing")
    public DfResponse<Listing> listing(@RequestParam String orderType,
                                       @RequestParam Integer page,
                                       @RequestParam Integer size){

        return DfResponse.okEntity(
            oderService.listing(orderType, page, size)
        );
    }


}
