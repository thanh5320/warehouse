package com.hust.movie_review.repositories;

import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.MetaDataWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICellRepository extends JpaRepository<Cell, Integer> {

}