package com.hust.movie_review.repositories;

import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.OrderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrderTypeRepository extends JpaRepository<OrderType, Integer> {
    public OrderType findByName(String name);
}