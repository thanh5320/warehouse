package com.hust.movie_review.repositories;

import com.hust.movie_review.models.MetaDataWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMetaDataWarehouseRepository extends JpaRepository<MetaDataWarehouse, Integer> {


//    Page<Movie> findMoviesByType(Pageable pageable, String type);
//    int countByType(String type);
//
//    Page<Movie> findByTitleContains(Pageable pageable, String search);
//    int countByTitleContains(String search);
}
