package com.hust.movie_review.repositories;

import com.hust.movie_review.models.Cell;
import com.hust.movie_review.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProductRepository extends JpaRepository<Product, Integer> {
    public List<Product> findByIdIn(List<Integer> ids);

//    @Query(value="select * from products a where a.name LIKE CONCAT('%', :keyword, '%') and cell_id is not " +
//        "null" +
//        " " +
//        "limit :size offset " +
//        ":offset",
//        nativeQuery=
//        true)
//    List<Product> pagingAndSearch(@Param("size") int size, int offset, String keyword);
//
//    @Query(value="select * from products a where cell_id is not " +
//        "null" +
//        " " +
//        "limit :size offset " +
//        ":offset",
//        nativeQuery=
//            true)
//    List<Product> paging(@Param("size") int size, int offset);
//
//    public Integer countByCellIsNotNullAndNameContains(String name);

    Page<Product> findProductByCellNotNull(Pageable pageable);

}
