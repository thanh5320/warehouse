#!/bin/bash
mvn clean install
docker build -t thanh-demo:v1.1.0 .
docker rm -f thanh-backend
docker run --name thanh-backend -v /etc/hosts:/etc/hosts:ro -d -p 8082:8082 --restart unless-stopped thanh-demo:v1.1.0
#docker push docker-daemon:5555/alert-tc2-backend:v1.1.$1